create database horarios_noturno;
use horarios_noturno;
create table Professor(
idProfessor int not null primary key,
nome varchar(100) not null);

create table Turma(
idTurma int not null primary key,
nome varchar(100) not null);

create table Aula(
idAula int not null primary key,
nome varchar(100) not null,
idProfessor int not null,
idTurma int not null,
constraint FK_Professor foreign key (idProfessor) references Professor(idProfessor),
constraint FK_Turma foreign key (idTurma) references Turma(idTurma));

CREATE TABLE Dia_da_Semana(
idDiaDaSemana int not null primary key,
descricao varchar(100) not null);
CREATE TABLE Periodo(
idPeriodo int not null primary key,
descricao varchar(100) not null);

CREATE TABLE Laboratorio(
idLaboratorio int not null primary key,
nome varchar(100) NOT NULL);

CREATE TABLE Aula_Laboratorio(
idLaboratorio int not null,
idAula int not null,
idDiaDaSemana int not null,
idPeriodo int not null,
primary key(idLaboratorio,idAula,idDiaDaSemana,idPeriodo),
constraint PFK_Laboratorio foreign key (idLaboratorio) references Laboratorio(idLaboratorio),
constraint PFK_Periodo foreign key (idPeriodo) references Periodo(idPeriodo),
constraint PFK_Aula foreign key (idAula) references Aula(idAula),
constraint PFK_DiaDaSemana foreign key (idDiaDaSemana) references Dia_da_Semana(idDiaDaSemana));