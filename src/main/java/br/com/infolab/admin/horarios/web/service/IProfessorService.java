package br.com.infolab.admin.horarios.web.service;
import br.com.infolab.admin.horarios.web.entidades.Professor;
import com.google.common.base.Optional;
public interface IProfessorService extends IGenericService<Professor, Integer>{
    public Optional<Professor> findAndInsertProfessorIfInexistente(Professor professor);
}
