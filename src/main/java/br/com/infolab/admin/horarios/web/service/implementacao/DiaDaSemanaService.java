package br.com.infolab.admin.horarios.web.service.implementacao;
import br.com.infolab.admin.horarios.web.entidades.DiaDaSemana;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.IDiaDaSemanaService;
import com.google.common.base.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
@Stateless
public class DiaDaSemanaService extends GenericService<DiaDaSemana, Integer> implements IDiaDaSemanaService{
    
    public DiaDaSemanaService(){
        super(DiaDaSemana.class);
    }

    @Override
    public Optional<DiaDaSemana> findAndInsertDiaDaSemanaIfInexistente(DiaDaSemana diaDaSemana) {
        TypedQuery<DiaDaSemana> query = 
                em.createQuery("select a from DiaDaSemana a "
                        + "where a.Descricao = :desc"
                        ,DiaDaSemana.class);
        query.setMaxResults(1);
        query.setParameter("desc", diaDaSemana.getDescricao());
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(diaDaSemana);
            return findAndInsertDiaDaSemanaIfInexistente(diaDaSemana);
        }catch(Exception ex){
            Logger.getLogger(DiaDaSemanaService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }
    
}
