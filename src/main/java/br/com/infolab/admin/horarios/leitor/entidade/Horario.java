package br.com.infolab.admin.horarios.leitor.entidade;
import br.com.infolab.admin.horarios.leitor.enums.DiaEnum;
import br.com.infolab.admin.horarios.leitor.enums.PeriodoEnum;

public class Horario {
    private Laboratorio lab;
    private Aula aula;
    private DiaEnum dia;
    private PeriodoEnum periodo;

    public Laboratorio getLab() {
        return lab;
    }

    public void setLab(Laboratorio lab) {
        this.lab = lab;
    }

    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    public DiaEnum getDia() {
        return dia;
    }

    public void setDia(DiaEnum dia) {
        this.dia = dia;
    }

    public PeriodoEnum getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoEnum periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "Horario{" + "lab=" + lab + ", aula=" + aula + ", dia=" + dia + ", periodo=" + periodo + '}';
    }
    public String getProfessorName(){
        return aula.getProfessorName();
    }
    public String getTurmaName(){
        return aula.getTurmaNome();
    }
    public String getAulaName(){
        return aula.getNome();
        
    }
    public String getDiaDaSemanaName(){
        return dia.name();
    }
    public String getPeriodoName(){
        return periodo.name();
    }
    public String getLabName(){
        return lab.getNome();
    }
    
    
}
