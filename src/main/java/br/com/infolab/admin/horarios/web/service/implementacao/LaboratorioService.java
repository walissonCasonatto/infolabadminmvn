/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.service.implementacao;

import br.com.infolab.admin.horarios.web.entidades.Laboratorio;
import br.com.infolab.admin.horarios.web.restful.LabVo;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.ILaboratorioService;
import com.google.common.base.Optional;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sandro
 */
@Stateless
public class LaboratorioService extends GenericService<Laboratorio, Integer> implements ILaboratorioService{
    
    public LaboratorioService(){
        super(Laboratorio.class);
    }

    @Override
    public Optional<Laboratorio> findAndInsertLaboratorioIfInexistente(Laboratorio lab) {
        TypedQuery<Laboratorio> query = 
                em.createQuery("select a from Laboratorio a where "
                        + "a.Nome = :nome"
                        ,Laboratorio.class);
        query.setMaxResults(1);
        query.setParameter("nome", lab.getNome());
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(lab);
            return findAndInsertLaboratorioIfInexistente(lab);
        }catch(Exception ex){
            Logger.getLogger(LaboratorioService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }

    @Override
    public List<LabVo> getAllLabNames() {
        TypedQuery<LabVo> query
                = em.createQuery("select new br.com.infolab.admin.horarios.web.restful.LabVo(l.Nome,l.IdLaboratorio) from Laboratorio l",LabVo.class);
        return query.getResultList();
    }
    
}
