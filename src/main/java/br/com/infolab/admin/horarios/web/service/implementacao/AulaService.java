/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.service.implementacao;

import br.com.infolab.admin.horarios.web.entidades.Aula;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.IAulaService;
import com.google.common.base.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sandro
 */
@Stateless
public class AulaService extends GenericService<Aula, Integer> implements IAulaService{
    
    public AulaService(){
        super(Aula.class);
    }

    @Override
    public Optional<Aula> findAndInsertAulaIfInexistente(Aula aula) {
        TypedQuery<Aula> query = em.createQuery(
                "SELECT A FROM Aula AS a WHERE a.nome = :nome"
                ,Aula.class);
        query.setParameter("nome", aula.getNome());
        query.setMaxResults(1);
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(aula);
            return findAndInsertAulaIfInexistente(aula);
        }catch(Exception ex){
            Logger.getLogger(AulaService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }
    
}
