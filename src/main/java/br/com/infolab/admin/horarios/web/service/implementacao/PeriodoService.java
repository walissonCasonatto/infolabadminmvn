/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.service.implementacao;

import br.com.infolab.admin.horarios.web.entidades.Periodo;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.IPeriodoService;
import com.google.common.base.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Sandro
 */
@Stateless
public class PeriodoService extends GenericService<Periodo, Integer> implements IPeriodoService{
    
    public PeriodoService(){
        super(Periodo.class);
    }

    @Override
    public Optional<Periodo> findAndInsertPeriodoIfInexistente(Periodo periodo) {
        TypedQuery<Periodo> query = em.createQuery("select p from Periodo p"
                + " where p.Descricao = :desc", Periodo.class);
        query.setMaxResults(1);
        query.setParameter("desc", periodo.getDescricao());
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(periodo);
            return findAndInsertPeriodoIfInexistente(periodo);
        }catch(Exception ex){
            Logger.getLogger(PeriodoService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }
    
}
