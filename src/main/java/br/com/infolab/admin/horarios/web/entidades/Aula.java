/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sandro
 */
@Entity
public class Aula {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdAula;
   
    @NotNull(message="Campo Obrigatorio")
    @Size(max=100,message="Nome pode conter até 100 caracteres")
    private String nome;
    
    @ManyToOne
    @JoinColumn(name="IdProfessor")
    private Professor professor;
    
    @ManyToOne
    @JoinColumn(name="IdTurma")
    private Turma turma;
    
//    @ManyToMany
//    @JoinTable(name="Aula_Laboratorio",
//            joinColumns=@JoinColumn(name="IdAula"),
//            inverseJoinColumns=@JoinColumn(name="IdLaboratorio"))
//    private List<Laboratorio> laboratorios;

    public Integer getIdAula() {
        return IdAula;
    }

    public void setIdAula(Integer IdAula) {
        this.IdAula = IdAula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

//    public List<Laboratorio> getLaboratorios() {
//        return laboratorios;
//    }
//
//    public void setLaboratorios(List<Laboratorio> laboratorios) {
//        this.laboratorios = laboratorios;
//    }


    
    
    
 
}
