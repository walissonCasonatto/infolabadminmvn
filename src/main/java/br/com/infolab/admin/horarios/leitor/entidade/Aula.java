package br.com.infolab.admin.horarios.leitor.entidade;

public class Aula {
    private String nome;
    private Professor professor;
    private Turma turma;

    public Aula() {
    }

    private Aula(String vaga) {
        this.nome = vaga;
        this.professor=new Professor();
        this.professor.setNome("Sem Professor");
        this.turma = new Turma();
        this.turma.setNome("Sem Turma");
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }
    public static Aula newAulaVaga(){
        return new Aula("vaga");
    }

    @Override
    public String toString() {
        return "Aula{" + "nome=" + nome + ", professor=" + professor + ", turma=" + turma + '}';
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aula other = (Aula) obj;
        if ((this.nome == null) ? (other.nome != null) : !this.nome.equals(other.nome)) {
            return false;
        }
        if(this.toString() == other.toString()){
            return false;
        }
        return true;
    }

    public String getProfessorName() {
        return professor.getNome();
    }
    public String getTurmaNome(){
        return turma.getNome();
    }
        
    
}
