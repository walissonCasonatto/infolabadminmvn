package br.com.infolab.admin.horarios.leitor.coletor;

import br.com.infolab.admin.horarios.leitor.enums.DiaEnum;
import br.com.infolab.admin.horarios.leitor.enums.PeriodoEnum;

public class ColletorDeConstantes {
        
    public static String DIA_SEGUNDA = "days=\"10000\"";
    public static String DIA_TERCA = "days=\"01000\"";
    public static String DIA_QUARTA = "days=\"00100\"";
    public static String DIA_QUINTA = "days=\"00010\"";
    public static String DIA_SEXTA = "days=\"00001\"";
    public static String DIA_TODODIA = "days=\"11111\"";
    public static String DIA_QUALQUERDIA = "days=\"10000,01000,00100,00010,00001\"";
    public static String PERIODO_UM = "period=\"1\"";
    public static String PERIODO_DOIS= "period=\"2\"";
    public static String PERIODO_TRES = "period=\"3\"";
    public static String PERIODO_QUATRO = "period=\"4\"";
    public static String PERIODO_CINCO = "period=\"5\"";
    
    
    
    public static String getParametroDia(DiaEnum dia) {
        switch (dia){
            case SEGUNDA:
                return DIA_SEGUNDA;
            case TERCA:
                return DIA_TERCA;
            case QUARTA:
                return DIA_QUARTA;
            case QUINTA:
                return DIA_QUINTA;    
            case SEXTA:
                return DIA_SEXTA;
            default:
                return "asdasdasdasdasdasdasdasd";
        }
    }
    public static String getParametroPeriodo(PeriodoEnum p){
        switch(p){
            case PRIMEIRO:
                return PERIODO_UM;
            case SEGUNDO:
                return PERIODO_DOIS;
            case TERCEIRO:
                return PERIODO_TRES;
            case QUARTO:
                return PERIODO_QUATRO;
            case QUINTO:
                return PERIODO_CINCO;
            default:
                return "asdfasdfasdf";
        }
        
    }
    
}
