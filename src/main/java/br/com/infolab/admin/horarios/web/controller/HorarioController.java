package br.com.infolab.admin.horarios.web.controller;

import java.util.List;
import javax.ejb.EJB;
import br.com.infolab.admin.horarios.leitor.entidade.Horario;
import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio;
import br.com.infolab.admin.horarios.web.entidades.DiaDaSemana;
import br.com.infolab.admin.horarios.web.entidades.Periodo;
import br.com.infolab.admin.horarios.web.service.IAulaService;
import br.com.infolab.admin.horarios.web.service.IAula_LaboratorioService;
import br.com.infolab.admin.horarios.web.service.IDiaDaSemanaService;
import br.com.infolab.admin.horarios.web.service.ILaboratorioService;
import br.com.infolab.admin.horarios.web.service.IPeriodoService;
import br.com.infolab.admin.horarios.web.service.IProfessorService;
import br.com.infolab.admin.horarios.web.service.ITurmaService;
import br.com.infolab.admin.horarios.web.entidades.*;
import com.google.common.base.Optional;
import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class HorarioController {

    @EJB
    private IProfessorService professorService;

    @EJB
    private ITurmaService turmaService;

    @EJB
    private IAulaService aulaService;

    @EJB
    private IAula_LaboratorioService aulaLabService;

    @EJB
    private ILaboratorioService labService;

    @EJB
    private IPeriodoService periodoService;

    @EJB
    private IDiaDaSemanaService diaService;

    public boolean persisteHorarios(List<Horario> horarios) {
        for (Horario h : horarios) {
            Optional<Aula> aula = insereAulaSeNecessario(h);
            Optional<Laboratorio> lab
                    = labService.findAndInsertLaboratorioIfInexistente(
                            new Laboratorio(h.getLabName()));
            Optional<DiaDaSemana> dia
                    = diaService.findAndInsertDiaDaSemanaIfInexistente(
                            new DiaDaSemana(h.getDiaDaSemanaName()));
            Optional<Periodo> periodo
                    = periodoService.findAndInsertPeriodoIfInexistente(
                            new Periodo(h.getPeriodoName()));
            Optional<Aula_Laboratorio> aulaLab = insereAulaLab(aula, lab, dia, periodo);
            if (!aulaLab.isPresent()) {
                return false;
            }

        }
        return true;

    }

    public String retornarDiaSemana(Date data) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
        return pesquisarDiaSemana(diaSemana);
    }

    public String pesquisarDiaSemana(int _diaSemana) {
        String diaSemana = null;
        switch (_diaSemana) {
            case 1: {
                diaSemana = "DOMINGO";
                break;
            }
            case 2: {
                diaSemana = "SEGUNDA";
                break;
            }
            case 3: {
                diaSemana = "TERCA";
                break;
            }
            case 4: {
                diaSemana = "QUARTA";
                break;
            }
            case 5: {
                diaSemana = "QUINTA";
                break;
            }
            case 6: {
                diaSemana = "SEXTA";
                break;
            }
            case 7: {
                diaSemana = "SABADO";
                break;
            }
        }
        return diaSemana;
    }

    private Optional<Aula_Laboratorio> insereAulaLab(Optional<Aula> aula, Optional<Laboratorio> lab, Optional<DiaDaSemana> dia, Optional<Periodo> periodo) {
        if (aula.isPresent()
                && lab.isPresent()
                && dia.isPresent()
                && periodo.isPresent()) {
            return aulaLabService.findAndInsertAulaLabIfInexistente(
                    new Aula_Laboratorio(
                            lab.get(),
                            aula.get(),
                            dia.get(),
                            periodo.get()));
        } else {
            return Optional.absent();
        }
    }

    private Optional<Aula> insereAulaSeNecessario(Horario h) {

        Optional<Professor> professor
                = professorService.findAndInsertProfessorIfInexistente(new Professor(h.getProfessorName()));
        Optional<Turma> turma
                = turmaService.findAndInsertTurmaIfInexistente(new Turma(h.getTurmaName()));
        if (turma.isPresent() && professor.isPresent()) {
            Aula aulaDB = new Aula();
            aulaDB.setNome(h.getAulaName());
            aulaDB.setProfessor(professor.get());
            aulaDB.setTurma(turma.get());
            return aulaService.findAndInsertAulaIfInexistente(aulaDB);
        } else {
            return Optional.absent();
        }
    }

    public IProfessorService getProfessorService() {
        return professorService;
    }

    public void setProfessorService(IProfessorService professorService) {
        this.professorService = professorService;
    }

    public ITurmaService getTurmaService() {
        return turmaService;
    }

    public void setTurmaService(ITurmaService turmaService) {
        this.turmaService = turmaService;
    }

    public IAulaService getAulaService() {
        return aulaService;
    }

    public void setAulaService(IAulaService aulaService) {
        this.aulaService = aulaService;
    }

    public ILaboratorioService getLabService() {
        return labService;
    }

    public void setLabService(ILaboratorioService labService) {
        this.labService = labService;
    }

    public IAula_LaboratorioService getAulaLabService() {
        return aulaLabService;
    }

    public void setAulaLabService(IAula_LaboratorioService aulaLabService) {
        this.aulaLabService = aulaLabService;
    }

    public IPeriodoService getPeriodoService() {
        return periodoService;
    }

    public void setPeriodoService(IPeriodoService periodoService) {
        this.periodoService = periodoService;
    }

    public IDiaDaSemanaService getDiaService() {
        return diaService;
    }

    public void setDiaService(IDiaDaSemanaService diaService) {
        this.diaService = diaService;
    }

}
