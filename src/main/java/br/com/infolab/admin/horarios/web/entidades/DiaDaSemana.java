/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sandro
 */
@Entity
public class DiaDaSemana {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdDia;
    
    @NotNull(message="Campo Obrigatorio")
    @Size(max=100,message="Dia da Semana pode conter até 100 caracteres")
    private String Descricao;

    public DiaDaSemana() {
    }

    public DiaDaSemana(String Descricao) {
        this.Descricao = Descricao;
    }


    
    public Integer getIdDia() {
        return IdDia;
    }

    public void setIdDia(Integer IdDia) {
        this.IdDia = IdDia;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }
    
    
    
}
