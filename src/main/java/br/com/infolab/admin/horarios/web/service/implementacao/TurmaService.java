package br.com.infolab.admin.horarios.web.service.implementacao;
import br.com.infolab.admin.horarios.web.entidades.Turma;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.ITurmaService;
import com.google.common.base.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
@Stateless
public class TurmaService extends GenericService<Turma, Integer> implements ITurmaService{
    
    public TurmaService(){
        super(Turma.class);
    }

    @Override
    public Optional<Turma> findAndInsertTurmaIfInexistente(Turma turma) {
        TypedQuery<Turma> query = 
                em.createQuery("select t from Turma t "
                + "where t.nome = :nome",Turma.class);
        query.setMaxResults(1);
        query.setParameter("nome", turma.getNome());
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(turma);
            return findAndInsertTurmaIfInexistente(turma);
        }catch(Exception ex){
            Logger.getLogger(TurmaService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }
    
    
}
