package br.com.infolab.admin.horarios.web.restful;
public class AulaLabVO {
    private final String nomeLab;
    private final String nomeAula;
    private final String diaSemana;
    private final String periodo;
    private final String nomeProf;
    private final String nomeTurma;

    public String getNomeLab() {
        return nomeLab;
    }

    public String getNomeAula() {
        return nomeAula;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public String getPeriodo() {
        return periodo;
    }

    public String getNomeProf() {
        return nomeProf;
    }

    public String getNomeTurma() {
        return nomeTurma;
    }

    

    public AulaLabVO(String nomeLab, String nomeAula, String diaSemana, String periodo, String nomeProf, String nomeTurma) {
        this.nomeLab = nomeLab;
        this.nomeAula = nomeAula;
        this.diaSemana = diaSemana;
        this.periodo = periodo;
        this.nomeProf = nomeProf;
        this.nomeTurma = nomeTurma;
    }
    
    public static class Builder{
        private String nomeLab;
        private String nomeAula;
        private String diaSemana;
        private String periodo;
        private String nomeProf;
        private String nomeTurma;
        public Builder(){
            
        }
        
        public Builder nomeLab(String nomelab){
            this.nomeLab = nomelab;
            return this;
        }
        public Builder nomeAula(String nomeaula){
            this.nomeAula = nomeaula;
            return this;
        }
        public Builder diaSemana(String diasemana){
            this.diaSemana = diasemana;
            return this;
        }
        public Builder periodo(String periodo){
            this.periodo = periodo;
            return this;
        }
        public Builder nomeProf(String nomeProf){
            this.nomeProf = nomeProf;
            return this;
        }
        public Builder nomeTurma(String nometurma){
            this.nomeTurma = nometurma;
            return this;
        }
        public AulaLabVO Build(){
            return new AulaLabVO(nomeLab, nomeAula, diaSemana, periodo, nomeProf, nomeTurma);
        }
        
    }
    
    
}
