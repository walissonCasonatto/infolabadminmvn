/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sandro
 */
@Entity
public class Professor {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdProfessor;
    
    @NotNull(message="Campo Obrigatorio")
    @Size(max=100,message="Nome pode conter até 100 caracteres")
    private String nome;
    
    @OneToMany(mappedBy="professor")
    private List<Aula> aula;

    public Integer getIdProfessor() {
        return IdProfessor;
    }

    public Professor() {
    }

    public Professor(String nome) {
        this.nome = nome;
    }
    

    public void setIdProfessor(Integer IdProfessor) {
        this.IdProfessor = IdProfessor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Aula> getAula() {
        return aula;
    }

    public void setAula(List<Aula> aula) {
        this.aula = aula;
    }
    
    
    
}
