package br.com.infolab.admin.horarios.web.service;
import br.com.infolab.admin.horarios.web.entidades.Periodo;
import com.google.common.base.Optional;
public interface IPeriodoService extends IGenericService<Periodo, Integer>{
    public Optional<Periodo> findAndInsertPeriodoIfInexistente(Periodo periodo);
}
