/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.leitor.entidade;

import br.com.infolab.admin.horarios.leitor.enums.PeriodoEnum;

/**
 *
 * @author a12027447
 */
public class Periodo {
    private Aula aula;
    private PeriodoEnum horario;
    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    public PeriodoEnum getHorario() {
        return horario;
    }

    public void setHorario(PeriodoEnum horario) {
        this.horario = horario;
    }

    public Periodo() {
        aula = new Aula();
        horario = PeriodoEnum.PRIMEIRO;
    }

    public Periodo(PeriodoEnum horario) {
        this.horario = horario;
    }
    
    public static Periodo instanceOf(PeriodoEnum p){
        return new Periodo(p);
        
    }

    @Override
    public String toString() {
        return "Periodo "+ (horario.ordinal()+1);
    }
    
    
    
    
}
