package br.com.infolab.admin.horarios.web.entidades;
import java.io.Serializable;
import javax.persistence.Embeddable;
@Embeddable
public class Aula_Laboratorio_Id implements Serializable{
    
    private int IdLaboratorio;
    
    private int IdAula;
    
    private int IdDia;
    
    private int idPeriodo;

    public int getIdLaboratorio() {
        return IdLaboratorio;
    }

    public void setIdLaboratorio(int IdLaboratorio) {
        this.IdLaboratorio = IdLaboratorio;
    }

    public int getIdAula() {
        return IdAula;
    }

    public void setIdAula(int IdAula) {
        this.IdAula = IdAula;
    }

    public int getIdDia() {
        return IdDia;
    }

    public void setIdDia(int IdDia) {
        this.IdDia = IdDia;
    }

    public int getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(int idPeriodo) {
        this.idPeriodo = idPeriodo;
    }
    
    
    
}
