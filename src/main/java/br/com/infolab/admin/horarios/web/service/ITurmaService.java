package br.com.infolab.admin.horarios.web.service;
import br.com.infolab.admin.horarios.web.entidades.Turma;
import com.google.common.base.Optional;
public interface ITurmaService extends IGenericService<Turma, Integer>{
     public Optional<Turma> findAndInsertTurmaIfInexistente(Turma turma);
}
