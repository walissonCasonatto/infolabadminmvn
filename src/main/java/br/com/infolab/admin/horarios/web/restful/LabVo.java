
package br.com.infolab.admin.horarios.web.restful;

public class LabVo {
    private String lab;
    private int id;

    public LabVo(String lab, Integer id) {
        this.lab = lab;
        this.id = id;
    }

    
    
    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

}
