/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.leitor.entidade;

import java.util.ArrayList;
import java.util.List;
import br.com.infolab.admin.horarios.leitor.enums.DiaEnum;

/**
 *
 * @author a12027447
 */
public class DayOfWeek {
    private DiaEnum dia;
    private List<Periodo> periodos;

    public DiaEnum getDia() {
        return dia;
    }
    public static DayOfWeek instanceOF(DiaEnum d){
        return new DayOfWeek(d);
        }

    public void setDia(DiaEnum dia) {
        this.dia = dia;
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }

    public DayOfWeek() {
        dia = DiaEnum.QUALQUER_DIA;
        periodos = new ArrayList<Periodo>();
    }

    public DayOfWeek(DiaEnum dia) {
        this.dia = dia;
        periodos = new ArrayList<Periodo>();
    }

    @Override
    public String toString() {
        switch(dia){
            case SEGUNDA:
                return "Segunda";
            case TERCA:
                return "terca";
            case QUARTA:
                return "Quarta";
            case QUINTA:
                return "Quinta";
            case SEXTA:
                return "Sexta";
            case CADA_DIA:
                return "Cada Dia";
            case QUALQUER_DIA:
                return "Qualquer DIa";
            default:
                return "Dia Desconhecido";
        }
    }
    
    
    
    
}
