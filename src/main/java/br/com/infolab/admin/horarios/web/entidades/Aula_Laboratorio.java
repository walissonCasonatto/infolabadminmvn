package br.com.infolab.admin.horarios.web.entidades;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
@Entity
public class Aula_Laboratorio {
    
    @EmbeddedId
    private Aula_Laboratorio_Id id;

    public Aula_Laboratorio() {
    }

    public Aula_Laboratorio(Laboratorio laboratorio, Aula aula, DiaDaSemana diaDaSemana, Periodo periodo) {
        this.laboratorio = laboratorio;
        this.aula = aula;
        this.diaDaSemana = diaDaSemana;
        this.periodo = periodo;
        this.id = new Aula_Laboratorio_Id();
        this.id.setIdAula(aula.getIdAula());
        this.id.setIdDia(diaDaSemana.getIdDia());
        this.id.setIdLaboratorio(laboratorio.getIdLaboratorio());
        this.id.setIdPeriodo(periodo.getIdPeriodo());
    }
    
    
    

    @ManyToOne
    @JoinColumn(name = "IdLaboratorio", insertable = false, updatable = false)
    @NotNull(message = "Campo Obrigatorio")
    private Laboratorio laboratorio;
    
    @ManyToOne
    @JoinColumn(name = "IdAula", insertable = false, updatable = false)
    @NotNull(message = "Campo Obrigatorio")
    private Aula aula;
    
    @ManyToOne
    @JoinColumn(name = "IdDia", insertable = false, updatable = false)
    @NotNull(message = "Campo Obrigatorio")
    private DiaDaSemana diaDaSemana;
    
    @ManyToOne
    @JoinColumn(name = "idPeriodo", insertable = false, updatable = false)
    @NotNull(message = "Campo Obrigatorio")
    private Periodo periodo;

    public Aula_Laboratorio_Id getId() {
        return id;
    }

    public void setId(Aula_Laboratorio_Id id) {
        this.id = id;
    }

    public Laboratorio getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(Laboratorio laboratorio) {
        this.laboratorio = laboratorio;
    }

    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    public DiaDaSemana getDiaDaSemana() {
        return diaDaSemana;
    }

    public void setDiaDaSemana(DiaDaSemana diaDaSemana) {
        this.diaDaSemana = diaDaSemana;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }
    
    
    
    
}
