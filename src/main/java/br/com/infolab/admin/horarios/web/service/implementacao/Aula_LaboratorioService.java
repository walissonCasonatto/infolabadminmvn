package br.com.infolab.admin.horarios.web.service.implementacao;
import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio;
import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio_Id;
import br.com.infolab.admin.horarios.web.service.IAula_LaboratorioService;
import com.google.common.base.Optional;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

@Stateless
public class Aula_LaboratorioService extends GenericService<Aula_Laboratorio, Aula_Laboratorio_Id> implements IAula_LaboratorioService{

    public Aula_LaboratorioService() {
        super(Aula_Laboratorio.class);
    }

    @Override
    public Optional<Aula_Laboratorio> findAndInsertAulaLabIfInexistente(Aula_Laboratorio aulaLab) {
        TypedQuery<Aula_Laboratorio> query = em.createQuery(
                "SELECT A FROM Aula_Laboratorio AS a WHERE a.id = :id"
                ,Aula_Laboratorio.class);
        query.setParameter("id", aulaLab.getId());
        query.setMaxResults(1);
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(aulaLab);
            return findAndInsertAulaLabIfInexistente(aulaLab);
        }catch(Exception ex){
            Logger.getLogger(AulaService.class.getName()).log(Level.SEVERE,ex.getMessage());
            return Optional.absent();
        }
    }

    @Override
    public List<Aula_Laboratorio> laboratoriosVagos() {
        TypedQuery<Aula_Laboratorio> horariosQuery;
        horariosQuery = em.createQuery("select h from Aula_Laboratorio h"
                + " WHERE h.aula.nome like 'vaga'", Aula_Laboratorio.class);
        return horariosQuery.getResultList();
    }

    @Override
    public List<Aula_Laboratorio> horariosHoje() {
        Calendar data = Calendar.getInstance();
        TypedQuery<Aula_Laboratorio> horariosQuery;
        horariosQuery = em.createQuery("select h from Aula_Laboratorio h where h.diaDaSemana.IdDia = :idDia ORDER BY h.laboratorio.Nome , h.periodo.IdPeriodo",Aula_Laboratorio.class);
        horariosQuery.setParameter("idDia", data.get(data.DAY_OF_WEEK));
        return horariosQuery.getResultList();
    }

    @Override
    public List<Aula_Laboratorio> horariosLaboratorio(String id) {
        TypedQuery<Aula_Laboratorio> horariosQuery;
        horariosQuery = em.createQuery("select h from Aula_Laboratorio h "
                + "WHERE h.laboratorio.IdLaboratorio = :idlab ORDER BY h.laboratorio.Nome , h.periodo.IdPeriodo", Aula_Laboratorio.class);
        horariosQuery.setParameter("idlab", Integer.parseInt(id));
        return horariosQuery.getResultList();
    }

    @Override
    public List<Aula_Laboratorio> horariosDia(String id) {
        TypedQuery<Aula_Laboratorio> horariosQuery;
        horariosQuery = em.createQuery("select h from Aula_Laboratorio h WHERE h.diaDaSemana.IdDia = :id ORDER BY h.laboratorio.Nome , h.periodo.IdPeriodo", Aula_Laboratorio.class);
        horariosQuery.setParameter("id", Integer.parseInt(id));
        return horariosQuery.getResultList();
    }

    @Override
    public List<Aula_Laboratorio> horariosDoLabNoDia(String lab, String id) {
        TypedQuery<Aula_Laboratorio> horariosQuery;
        horariosQuery = em.createQuery("select h from Aula_Laboratorio h "
                + "WHERE h.laboratorio.IdLaboratorio = :idlab "
                + "and h.diaDaSemana.IdDia = :id "
                + "ORDER BY h.laboratorio.Nome , h.periodo.IdPeriodo", Aula_Laboratorio.class);
        horariosQuery.setParameter("idlab", Integer.parseInt(lab));
        horariosQuery.setParameter("id", Integer.parseInt(id));
        return horariosQuery.getResultList();
    }
    
  
    
}
