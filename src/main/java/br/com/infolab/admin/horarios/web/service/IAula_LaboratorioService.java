package br.com.infolab.admin.horarios.web.service;

import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio;
import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio_Id;
import com.google.common.base.Optional;
import java.util.List;

public interface IAula_LaboratorioService extends IGenericService<Aula_Laboratorio, Aula_Laboratorio_Id>{
    public Optional<Aula_Laboratorio> findAndInsertAulaLabIfInexistente(Aula_Laboratorio aulaLab);
    public List<Aula_Laboratorio> laboratoriosVagos();
    public List<Aula_Laboratorio> horariosHoje();
    public List<Aula_Laboratorio> horariosLaboratorio(String nome);
    public List<Aula_Laboratorio> horariosDia(String id);

    public List<Aula_Laboratorio> horariosDoLabNoDia(String lab, String id);
    
}
