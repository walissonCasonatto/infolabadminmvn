/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sandro
 */
@Entity
public class Turma {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdTurma;
    
    @NotNull(message="Campo Obrigatorio")
    @Size(max=100,message="Nome pode conter até 100 caracteres")
    private String nome;
    
    @OneToMany(mappedBy="turma")
    private List<Aula> aula;

    public Turma(String nome) {
        this.nome = nome;
    }

    public Turma() {
    }

    

    public Integer getIdTurma() {
        return IdTurma;
    }

    public void setIdTurma(Integer IdTurma) {
        this.IdTurma = IdTurma;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Aula> getAula() {
        return aula;
    }

    public void setAula(List<Aula> aula) {
        this.aula = aula;
    }
    
    
}
