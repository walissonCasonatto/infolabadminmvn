package br.com.infolab.admin.horarios.web.restful;

import br.com.infolab.admin.horarios.web.entidades.Aula_Laboratorio;
import br.com.infolab.admin.horarios.web.service.IAula_LaboratorioService;
import br.com.infolab.admin.horarios.web.service.ILaboratorioService;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/labs")
@Stateless
public class LabInfoResource {
    @EJB
    private IAula_LaboratorioService aulaLabService;
    @EJB
    private ILaboratorioService labService;
    
    
    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo ui){
        List<Aula_Laboratorio> aulalab = aulaLabService.todos();
        return retornaLista(aulalab);
    }
    @Path("/hoje")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToday(@Context UriInfo ui){
        List<Aula_Laboratorio> aulalab = aulaLabService.horariosHoje();
        return retornaLista(aulalab);
    }

    
    
    @Path("/dia/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDia(@PathParam("id") String id){
        List<Aula_Laboratorio> aulalab = aulaLabService.horariosDia(id);
        return retornaLista(aulalab);
    }
    @Path("/lab/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLab(@PathParam("id") String id){
        List<Aula_Laboratorio> aulalab = aulaLabService.horariosLaboratorio(id);
        return retornaLista(aulalab);
    }
    
    @Path("/dia/{id}/{lab}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDiaAndLab(@PathParam("id") String id,@PathParam("lab") String lab){
        List<Aula_Laboratorio> aulalab = aulaLabService.horariosDoLabNoDia(lab,id);
        return retornaLista(aulalab);
    }
    @Path("/labs")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLabs(){
        List<LabVo> labNames = labService.getAllLabNames();
        String retorno = new Gson().toJson(labNames);
        return Response.ok(retorno).build();
    }
    
    
    private Response retornaLista(List<Aula_Laboratorio> aulalab) {
        List<AulaLabVO> aulas = new ArrayList<AulaLabVO>();
        for(Aula_Laboratorio aula :aulalab){
            aulas.add(buildAula(aula));
        }
        String retorno = (new Gson()).toJson(aulas);
        return Response.ok(retorno).build();
    }
    
    
    
    
    private AulaLabVO buildAula(Aula_Laboratorio aula){
        return new AulaLabVO.Builder()
                .nomeAula(aula.getAula().getNome())
                .diaSemana(aula.getDiaDaSemana().getDescricao())
                .nomeLab(aula.getLaboratorio().getNome())
                .nomeProf(aula.getAula().getProfessor().getNome())
                .nomeTurma(aula.getAula().getTurma().getNome())
                .periodo(aula.getPeriodo().getDescricao())
                .Build();
    }
}
