/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.service;

import br.com.infolab.admin.horarios.web.entidades.Aula;
import com.google.common.base.Optional;

/**
 *
 * @author Sandro
 */
public interface IAulaService extends IGenericService<Aula, Integer>{
    
    public Optional<Aula> findAndInsertAulaIfInexistente(Aula aula);
}
