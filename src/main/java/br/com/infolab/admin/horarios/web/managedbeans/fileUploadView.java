package br.com.infolab.admin.horarios.web.managedbeans;
 

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import br.com.infolab.admin.horarios.leitor.entidade.Horario;
import br.com.infolab.admin.horarios.leitor.leitor.Leitor;
import org.primefaces.model.UploadedFile;
import br.com.infolab.admin.horarios.web.controller.HorarioController;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@SessionScoped
@ManagedBean
public class fileUploadView implements Serializable{
    
    @Inject
    private HorarioController horarioController;
     
    private UploadedFile file;
 
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    public void upload() {
        if(file != null) {
            try {
                Leitor leitor = new Leitor(file.getContents());
                List<Horario> horarios = leitor.getHorarios();
                if(horarios.size()<1){
                    return;
                }
                boolean sucesso = horarioController.persisteHorarios(horarios);
                if(sucesso){
                    sendMessage("Sucesso","XML salvo Com Sucesso");
                }else{
                    sendMessage("Falha", "Houve um erro ao salvar o xml");
                }
            } catch (Exception ex) {
                Logger.getLogger(fileUploadView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void sendMessage(String status,String mensagem) {
        FacesMessage message = new FacesMessage(
                status, mensagem);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}