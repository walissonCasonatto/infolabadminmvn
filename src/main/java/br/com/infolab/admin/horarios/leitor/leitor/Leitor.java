
package br.com.infolab.admin.horarios.leitor.leitor;
import br.com.infolab.admin.horarios.leitor.enums.PeriodoEnum;
import br.com.infolab.admin.horarios.leitor.enums.DiaEnum;
import br.com.infolab.admin.horarios.leitor.entidade.Horario;
import br.com.infolab.admin.horarios.leitor.entidade.Laboratorio;
import br.com.infolab.admin.horarios.leitor.entidade.Turma;
import br.com.infolab.admin.horarios.leitor.entidade.Aula;
import br.com.infolab.admin.horarios.leitor.entidade.Professor;
import java.io.*;
import java.util.*;
import br.com.infolab.admin.horarios.leitor.coletor.ColletorDeConstantes;

public class Leitor {    
    private final File arquivo;
    private BufferedReader leitor;
    public Leitor(byte[] arq) throws FileNotFoundException, UnsupportedEncodingException, IOException{
        arquivo = new File("HorariosNoturno");
        FileOutputStream fileOuputStream = 
                  new FileOutputStream(arquivo); 
        fileOuputStream.write(arq);
        fileOuputStream.close();
        leitor = geraLeitor(arquivo.getAbsolutePath());
        
    }
    
    private BufferedReader geraLeitor(String patch) throws UnsupportedEncodingException, FileNotFoundException{
        BufferedReader leitor = new BufferedReader(
                new InputStreamReader(
                new FileInputStream(patch),
                        "ISO-8859-1"));
        return leitor;
    }
    
    
    public List<Horario> getHorarios() throws IOException{
        leitor = geraLeitor(arquivo.getAbsolutePath());
        List<Horario> horarios = new ArrayList();
        List<Laboratorio> labs = new ArrayList<Laboratorio>();
        while (true) {
            String linha;
            linha=leitor.readLine();
            if (linha == null) {
                break;
            }
            if(linha.contains("Lab. de Info")){
                Laboratorio c = new Laboratorio();
                c.setNome(linha.replaceAll(".* name=\"", "").replaceFirst("\".*", ""));
                labs.add(c);
            }
            
        }
        if(labs.size()<1){
            return horarios;
        }
        leitor.close();
        for(Laboratorio b : labs){
            horarios.addAll(getHorariosPorLab(b));
        }
        return horarios;
    }    
    

    private Aula getAula(String b, DiaEnum dia, PeriodoEnum p) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        List<String> lessonIds = new ArrayList<String>();
        Aula aula = new Aula();
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<card")){
                if(linha.contains(b)){
                    if(linha.contains(ColletorDeConstantes.getParametroDia(dia))){
                        if(linha.contains(ColletorDeConstantes.getParametroPeriodo(p))){
                            lessonIds.add(linha.replaceAll(".* lessonid=\"", "").replaceFirst("\".*", ""));
                        }
                    }
                }
            }
        }
        leitor.close();
        if(!lessonIds.isEmpty()){
            if(lessonIds.size() > 1){
                System.out.println("dados incoerentes em " +b + " " + dia.toString() + " "+ p.toString());
            }else{
                aula.setNome(getNomeAulaByLessonId(lessonIds.get(0)));
                aula.setProfessor(getProfessorByLessonID(lessonIds.get(0)));
                aula.setTurma(getTurmaByLessonId(lessonIds.get(0)));
            }
        }else{
            aula = Aula.newAulaVaga();
        }
        
        return aula;
    }

    private Professor getProfessorByLessonID(String idLesson) throws IOException {
       leitor = geraLeitor(arquivo.getAbsolutePath());
        String professorId = "";
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<lesson")){
                if(linha.contains(idLesson)){
                    professorId = linha.replaceAll(".* teacherids=\"", "").replaceFirst("\".*", "");
                }
            }
        }
        leitor.close();
        return getProfessorByID(professorId);
    }

    private Professor getProfessorByID(String professorId) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        Professor p = new Professor();
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<teacher")){
                if(linha.contains(professorId)){
                    p.setNome(linha.replaceAll(".* name=\"", "").replaceFirst("\".*", ""));
                }
            }
        }
        leitor.close();
        return p;
    }

    private String getNomeAulaByLessonId(String lessonId) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        String subjectId = "";
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<lesson")){
                if(linha.contains(lessonId)){
                    subjectId = linha.replaceAll(".* subjectid=\"", "").replaceFirst("\".*", "");
                }
            }
        }
        leitor.close();
        return getSubjectByID(subjectId);
    }

    private String getSubjectByID(String subjectId) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        String nome = "nome da aula nao encontrado";
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<subject")){
                if(linha.contains(subjectId)){
                    nome = linha.replaceAll(".* name=\"","").replaceFirst("\".*", "");
                }
            }
        }
        leitor.close();
        return nome;
}

    private Turma getTurmaByLessonId(String idLesson) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        String classId = "";
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<lesson")){
                if(linha.contains(idLesson)){
                    classId = linha.replaceAll(".* classids=\"", "").replaceFirst("\".*", "");
                }
            }
        }
        leitor.close();
        
        return getClassById(classId);
    }

    private Turma getClassById(String classId) throws IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        Turma t = new Turma();
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            
            if (linha == null) {
                break;
            }
            if(linha.contains("<class")){
                if(linha.contains(classId)){
                    t.setNome(linha.replaceAll(".* name=\"","").replaceFirst("\".*", ""));
                }
            }
        }
        leitor.close();
        return t;
    }

    private Collection<? extends Horario> getHorariosPorLab(Laboratorio b) 
            throws UnsupportedEncodingException, FileNotFoundException, IOException {
        List<Horario> horarios  = new ArrayList();
        String idlab = getIdLab(b);
        for(DiaEnum d : DiaEnum.values()){
            for(PeriodoEnum p : PeriodoEnum.values()){
                Horario horario = new Horario();
                horario.setLab(b);
                horario.setPeriodo(p);
                horario.setDia(d);
                horario.setAula(getAula(idlab,d,p));
                horarios.add(horario);
            }
        }
        return horarios;
    }

    private String getIdLab(Laboratorio b) 
            throws UnsupportedEncodingException, FileNotFoundException, IOException {
        leitor = geraLeitor(arquivo.getAbsolutePath());
        String idlab = "";
        while (true) {
            String linha = new String();
            linha=leitor.readLine();
            if (linha == null) {
                break;
            }
            if(linha.contains(b.getNome())){
                idlab =linha.replaceAll(".* id=\"", "").replaceFirst("\".*", "");               
            }
        }
        leitor.close();
        return idlab;
    }

    

   

}