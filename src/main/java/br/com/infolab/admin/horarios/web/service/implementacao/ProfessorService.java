package br.com.infolab.admin.horarios.web.service.implementacao;
import br.com.infolab.admin.horarios.web.entidades.Professor;
import javax.ejb.Stateless;
import br.com.infolab.admin.horarios.web.service.IProfessorService;
import com.google.common.base.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
@Stateless
public class ProfessorService extends GenericService<Professor, Integer> implements IProfessorService{
    
    public ProfessorService(){
        super(Professor.class);
    }

    @Override
    public Optional<Professor> findAndInsertProfessorIfInexistente(Professor professor) {
        TypedQuery<Professor> query =
                em.createQuery("select p from Professor p"
                + " where p.nome = :nome",Professor.class);
        query.setMaxResults(1);
        query.setParameter("nome", professor.getNome());
        try{
            return Optional.of(query.getSingleResult());
        }catch(NoResultException ex){
            salvar(professor);
            return findAndInsertProfessorIfInexistente(professor);
        }catch(Exception ex){
            Logger.getLogger(ProfessorService.class.getName()).log(Level.SEVERE, ex.getMessage());
            return Optional.absent();
        }
    }
    
}
