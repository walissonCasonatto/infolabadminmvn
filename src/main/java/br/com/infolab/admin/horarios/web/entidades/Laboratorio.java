/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infolab.admin.horarios.web.entidades;

import java.text.Normalizer;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Sandro
 */
@Entity
public class Laboratorio {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdLaboratorio;
    
    @NotNull(message="Campo Obrigatorio")
    @Size(max=100,message="Nome pode conter até 100 caracteres")
    private String Nome;

    public Laboratorio() {
    }

    public Laboratorio(String Nome) {
        this.Nome = Nome;
        normalizaNome();
    }
    
    
    
//    @ManyToMany(mappedBy="laboratorios")
//    private List<Aula> aulas;

    public Integer getIdLaboratorio() {
        return IdLaboratorio;
    }

    public void setIdLaboratorio(Integer IdLaboratorio) {
        this.IdLaboratorio = IdLaboratorio;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

//    public List<Aula> getAulas() {
//        return aulas;
//    }
//
//    public void setAulas(List<Aula> aulas) {
//        this.aulas = aulas;
//    }

    private void normalizaNome() {
        Nome = Normalizer.normalize(Nome, Normalizer.Form.NFD);
        Nome = Nome.replaceAll("[^\\p{ASCII}]", "");
        Nome = Nome.replaceAll("\\p{M}", "");
    }
    
    
}
