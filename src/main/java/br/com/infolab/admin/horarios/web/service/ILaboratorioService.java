package br.com.infolab.admin.horarios.web.service;
import br.com.infolab.admin.horarios.web.entidades.Laboratorio;
import br.com.infolab.admin.horarios.web.restful.LabVo;
import com.google.common.base.Optional;
import java.util.List;
public interface ILaboratorioService extends IGenericService<Laboratorio, Integer>{
    public Optional<Laboratorio> findAndInsertLaboratorioIfInexistente(Laboratorio lab);

    public List<LabVo> getAllLabNames();

}
