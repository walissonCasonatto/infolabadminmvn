package br.com.infolab.admin.horarios.web.service;
import br.com.infolab.admin.horarios.web.entidades.DiaDaSemana;
import com.google.common.base.Optional;
public interface IDiaDaSemanaService extends IGenericService<DiaDaSemana, Integer>{
     public Optional<DiaDaSemana> findAndInsertDiaDaSemanaIfInexistente(DiaDaSemana diaDaSemana);
}
